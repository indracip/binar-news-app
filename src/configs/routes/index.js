import {
  createStackNavigator,
  TransitionSpecs,
  TransitionPresets,
} from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { HomePage, SearchPage, NewsDetail, CategoryPage } from 'pages';

import { React, createBottomTabNavigator, FontAwesome5 } from 'libraries';
import { Color, FONTS } from 'utils';

const Stack = createStackNavigator();

/*--------------------------
     Transition Options
---------------------------
*/

const CustomTransition = {
  headerShown: false,
  transitionSpec: {
    open: TransitionSpecs.TransitionIOSSpec,
    close: TransitionSpecs.TransitionIOSSpec,
  },
  ...TransitionPresets.SlideFromRightIOS,
};

const BottomTab = createBottomTabNavigator();

/*--------------------------
     Bottom Navigation
---------------------------
*/

function BottomNavigation() {
  return (
    <BottomTab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: 'white',
        activeBackgroundColor: Color.primaryColor,
        labelStyle: {
          fontFamily: FONTS.quickSand,
        },
      }}>
      <BottomTab.Screen
        name="Home"
        component={HomePage}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: (tabInfo) => (
            <FontAwesome5 name={'home'} color={tabInfo.color} size={20} />
          ),
        }}
      />
      <BottomTab.Screen
        name="Search"
        component={SearchPage}
        options={{
          tabBarLabel: 'Search',
          tabBarIcon: (tabInfo) => (
            <FontAwesome5 name={'search'} color={tabInfo.color} size={20} />
          ),
        }}
      />
    </BottomTab.Navigator>
  );
}

/*--------------------------
     Routes
---------------------------
*/

class Routes extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="Welcome"
          screenOptions={CustomTransition}>
          <Stack.Screen name="Welcome" component={BottomNavigation} />
          <Stack.Screen name="NewsDetail" component={NewsDetail} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

export default Routes;
