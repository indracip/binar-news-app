import ApiRequest from './config';
import baseUrl from './url';

const API = {};

API.searchNewsN = ApiRequest.get(baseUrl.news_api.searchNews);
API.headline = ApiRequest.get(baseUrl.news_api.headline);
export default API;
