const base_url_news = 'https://newsapi.org/v2';

const baseUrl = {
  news_api: {
    headline: `${base_url_news}/top-headlines`,
    searchNews: `${base_url_news}/everything`,
  },
};

export default baseUrl;
