const styles = {
  container: {
    flex: 1,
  },
  button: {
    backgroundColor: 'white',
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 5,
    borderRadius: 25,
    minWidth: 350,
    minHeight: 50,
  },
  text: {
    fontSize: 20,
    fontWeight: 'bold',
  },
};

export default styles;
