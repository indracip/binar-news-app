import React from 'react';
import { View, Text, Pressable } from 'libraries';
import PropTypes from 'prop-types';
import styles from './style';
import { TouchableOpacity } from 'react-native-gesture-handler';

const Button = (props) => {
  const { title, onPress } = props;
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.button}>
        <Text style={styles.text}>{title}</Text>
      </View>
    </TouchableOpacity>
  );
};

Button.propTypes = {
  title: PropTypes.string,
  onPress: PropTypes.func,
};

Button.defaultProps = {
  title: '',
  onPress: () => {},
};

export default React.memo(Button);
