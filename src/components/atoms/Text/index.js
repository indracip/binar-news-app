import React from 'react';
import { View, Text as RNText } from 'react-native';
import { FONTS } from 'utils/styles';

const Text = ({ children, ...props }) => {
  return (
    <RNText {...props} style={{ ...props.style, fontFamily: FONTS.quickSand }}>
      {children}
    </RNText>
  );
};

export default Text;
