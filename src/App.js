/* eslint-disable react-native/no-inline-styles */
import { React } from 'libraries';
import { Routes } from 'configs';
import { SafeAreaProvider } from 'react-native-safe-area-context';

class App extends React.Component {
  render() {
    return (
      <SafeAreaProvider>
        <Routes />
      </SafeAreaProvider>
    );
  }
}

export default App;
