import { NetworkInfo, NetInfo } from 'libraries';

// Get Local IP
const getLocalIP = async () => {
  try {
    return await NetworkInfo.getIPAddress();
  } catch (error) {
    return Promise.resolve('');
  }
};

// Get IPv4 IP (priority: WiFi first, cellular second)
const getIPAddress = async () => {
  try {
    return await NetworkInfo.getIPV4Address();
  } catch (error) {
    return Promise.resolve('');
  }
};

// Get SSID
const getSSID = async () => {
  try {
    return await NetworkInfo.getSSID();
  } catch (error) {
    return Promise.resolve('');
  }
};

// Get BSSID
const getBSSID = async () => {
  try {
    return await NetworkInfo.getBSSID();
  } catch (error) {
    return Promise.resolve('');
  }
};

// Get Subnet
const getSubnet = async () => {
  try {
    return await NetworkInfo.getSubnet();
  } catch (error) {
    return Promise.resolve('');
  }
};

// Get Default Gateway IP
const getGatewayIPAddress = async () => {
  try {
    return await NetworkInfo.getGatewayIPAddress();
  } catch (error) {
    return Promise.resolve('');
  }
};

// Get frequency (supported only for Android)
const getFrequency = async () => {
  try {
    return await NetworkInfo.getFrequency();
  } catch (error) {
    return Promise.resolve('');
  }
};

const isWifi = async () => {
  try {
    const networkState = await NetInfo.fetch();
    return networkState.type === 'wifi';
  } catch (error) {
    return Promise.resolve(false);
  }
};

const isCelluler = async () => {
  try {
    const networkState = await NetInfo.fetch();
    return networkState.type === 'cellular';
  } catch (error) {
    return Promise.resolve(false);
  }
};

const getCellularGeneration = async () => {
  try {
    const networkState = await NetInfo.fetch();
    if (networkState.type === 'cellular') {
      return networkState.details.cellularGeneration;
    }
    return '';
  } catch (error) {
    return Promise.resolve('');
  }
};

const network = {
  isWifi,
  getSSID,
  getBSSID,
  getSubnet,
  isCelluler,
  getLocalIP,
  getIPAddress,
  getFrequency,
  getGatewayIPAddress,
  getCellularGeneration,
};

export default network;
