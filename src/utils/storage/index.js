import AsyncStorage from 'libraries';

const helper = {};

/**
 * @name set
 * @param {string} key
 * @param {string} value
 * @description set data to async storage (key,value) type
 */
helper.set = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value);
    return true;
  } catch (err) {
    return false;
  }
};

/**
 * @name get
 * @param {string} key
 * @description get data from async storage based on key
 */
helper.get = async (key) => {
  try {
    const value = await AsyncStorage.getItem(key);
    if (value !== null) return value;
    return false;
  } catch (err) {
    return false;
  }
};

/**
 * @name clear
 * @description clear data from async storage based on key
 */
helper.clear = async (key) => {
  try {
    await AsyncStorage.removeItem(key);
    return true;
  } catch (err) {
    return false;
  }
};

/**
 * @name clearAll
 * @description clear all data from async storage
 */
helper.clearAll = async () => {
  try {
    await AsyncStorage.clear();
    return true;
  } catch (err) {
    return false;
  }
};

/**
 * @name getAllKeys
 * @description get all data key from async storage
 */
helper.getAllKeys = async () => {
  try {
    const keys = await AsyncStorage.getAllKeys();
    return keys;
  } catch (err) {
    return false;
  }
};

helper.getMultiple = async (p) => {
  // p should be [first,second]
  try {
    const values = await AsyncStorage.multiGet(p);
    return values;
  } catch (err) {
    return false;
  }
};

helper.setMultiple = async (p) => {
  // p should be [first,second]
  try {
    await AsyncStorage.multiSet(p);
    return true;
  } catch (err) {
    return false;
  }
};

helper.clearMultiple = async (p) => {
  // p should be [first,second]
  try {
    await AsyncStorage.multiRemove(p);
    return true;
  } catch (err) {
    return false;
  }
};

export default helper;
