const appString = {
  global: {},
  pages: {
    home: {
      title: 'Headline News',
    },
    search: {
      placeholderText: 'Search News',
    },
    newsDetail: {
      title: 'News Detail',
    },
  },
};

export default appString;
