import {
  React,
  View,
  WebView,
  FontAwesome5,
  ActivityIndicator,
  useState,
  TouchableOpacity,
} from 'libraries';
import { Text } from 'components/atoms';
import styles from './style';
import { appString } from 'utils';

const NewsDetail = ({ route, navigation }) => {
  const { url } = route.params;
  const [isLoading, setLoading] = useState(true);
  return (
    <View style={styles.container}>
      <View style={styles.navbar}>
        <View style={styles.navbarContent}>
          <TouchableOpacity onPress={() => navigation.pop()}>
            <View style={{ padding: 5 }}>
              <FontAwesome5 name={'arrow-left'} color={'black'} size={20} />
            </View>
          </TouchableOpacity>
          <Text numberOfLines={1} style={styles.navbarTitle}>
            {appString.pages.newsDetail.title}
          </Text>
        </View>
      </View>
      <WebView
        source={{ uri: url }}
        onLoadStart={(syntheticEvent) => {
          setLoading(false);
        }}
        style={{ flex: 1 }}
      />
      {isLoading && (
        <View style={styles.loading}>
          <ActivityIndicator color="#009688" size="large" />
        </View>
      )}
    </View>
  );
};

export default NewsDetail;
