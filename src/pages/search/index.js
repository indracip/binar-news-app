import IMG from 'assets/images';
import API from 'configs/api';
import { API_KEY } from 'configs/const';

import {
  React,
  View,
  useState,
  useEffect,
  AppState,
  useRef,
  FlatList,
  Image,
  moment,
  FontAwesome5,
  StatusBar,
  SkeletonPlaceholder,
  TextInput,
  Dimensions,
  indicator,
  TouchableOpacity,
  HTML,
  FastImage,
} from 'libraries';
import { Text } from 'components/atoms';
import styles from './style';
import { appString, Color } from 'utils';

const SearchPage = ({ navigation }) => {
  const appState = useRef(AppState.currentState);
  const [dataNews, setDataNews] = useState([]);
  const [offset, setOffset] = useState(0);
  const [queryData, setQueryData] = useState('');
  const [loading, setLoading] = useState(false);
  const [endList, setEndList] = useState(false);

  const inputSearch = useRef(null);
  let onEndReachedCalledDuringMomentum;

  useEffect(() => {
    AppState.addEventListener('change', _handleAppStateChange);
    const unsubscribe = navigation.addListener('focus', () => {
      // The screen is focused
      inputSearch.current.focus();
    });

    //
    return () => {
      AppState.removeEventListener('change', _handleAppStateChange);
      unsubscribe;
    };
  }, [navigation]);

  const getNews = async ({ isReset }) => {
    try {
      await setLoading(true);
      if (isReset) {
        await setOffset(0);
        await setDataNews([]);
        await setEndList(false);
      }
      if (!endList || isReset) {
        await setLoading(true);
        const payload = {
          params: {
            apiKey: API_KEY.news_api,
            pageSize: 10,
            page: !isReset ? offset : 1,
            q: queryData,
          },
        };
        const data = await API.searchNewsN(payload);
        if (data) {
          if (isReset) {
            setOffset(1);
            setDataNews(data.articles);
          } else {
            setOffset(offset + 1);
            if (data.articles.length === 0) {
              setEndList(true);
            } else {
              setDataNews([...dataNews, ...data.articles]);
            }
          }
          await setLoading(false);
        }
      }
    } catch (e) {}
  };

  const _handleAppStateChange = (nextAppState) => {
    if (
      appState.current.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      console.log('App has come to the foreground!');
    }

    appState.current = nextAppState;
    console.log('AppState', appState.current);
  };

  const renderFooter = () => {
    return loading && !endList ? (
      <View style={{ alignSelf: 'center' }}>
        <indicator.DotsLoader color={Color.primaryColor} />
      </View>
    ) : (
      <View></View>
    );
  };

  const renderItem = ({ item }) => {
    const created_at_format = moment(new Date(item.publishedAt)).format('LLL');
    let imageUrl =
      'https://mtsn4malang.sch.id/wp-content/uploads/2019/08/placeholder-1.png';
    if (item.urlToImage != '' && item.urlToImage != null) {
      imageUrl = item.urlToImage;
    }
    return (
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('NewsDetail', {
            url: item.url,
          });
        }}>
        <View style={styles.item}>
          <View style={{ flex: 1 }}>
            <View style={styles.headerWrapper}>
              <View style={{ flex: 1 }}>
                <View style={{ minHeight: 60 }}>
                  <Text numberOfLines={3} style={styles.title}>
                    {item.title}
                  </Text>
                </View>

                <Text numberOfLines={3} style={styles.subtitle}>
                  {item.description}
                </Text>
              </View>
            </View>

            <View style={styles.footer}>
              <View>
                <Text style={styles.author}>Author: {item.author}</Text>
                <Text style={styles.date}>{created_at_format}</Text>
              </View>
            </View>
          </View>
          <View style={styles.sourceWrapper}>
            <Text style={styles.sourceText}>
              {item.source.name ? item.source.name : '-'}
            </Text>
          </View>
          <FastImage style={styles.image} source={{ uri: imageUrl }} />
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={'white'} barStyle={'dark-content'} />
      <View style={{ padding: 15 }}>
        <TextInput
          ref={inputSearch}
          placeholder={appString.pages.search.placeholderText}
          style={styles.search}
          onChangeText={(text) => {
            setQueryData(text);
          }}
          onSubmitEditing={() => {
            queryData != ''
              ? getNews({ isReset: true })
              : alert('keyword cannot blank!');
          }}
          returnKeyType={'search'}
        />
      </View>

      {dataNews.length > 0 ? (
        <FlatList
          data={dataNews}
          renderItem={renderItem}
          keyExtractor={(item, index) => index.toString()}
          ListFooterComponent={renderFooter}
          onMomentumScrollBegin={() => {
            onEndReachedCalledDuringMomentum = false;
          }}
          onEndReached={() => {
            if (!onEndReachedCalledDuringMomentum) {
              getNews({ isReset: false });
              onEndReachedCalledDuringMomentum = true;
            }
          }}
          onEndReachedThreshold={0.2}
        />
      ) : loading ? (
        Array.from({ length: 10 }).map((_, index) => (
          <View key={index} style={{ marginBottom: 15, padding: 10 }}>
            <SkeletonPlaceholder>
              <SkeletonPlaceholder.Item flexDirection="row">
                <SkeletonPlaceholder.Item
                  flex={1}
                  paddingHorizontal={10}
                  justifyContent={'space-between'}>
                  <SkeletonPlaceholder.Item
                    width="100%"
                    height={180}
                    borderRadius={6}
                  />
                </SkeletonPlaceholder.Item>
              </SkeletonPlaceholder.Item>
            </SkeletonPlaceholder>
          </View>
        ))
      ) : (
        <View style={styles.searchPlaceholderWrapper}>
          <Image
            source={IMG.searchImage}
            style={styles.searchPlaceholderImage}
          />
        </View>
      )}
    </View>
  );
};

export default SearchPage;
