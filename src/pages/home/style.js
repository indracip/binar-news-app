import { StyleSheet } from 'libraries';
import { Color } from 'utils';
const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    backgroundColor: 'white',
  },
  navbar: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 15,
  },
  item: {
    flex: 1,
    backgroundColor: '#DFEAEF',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 10,
    position: 'relative',
    marginTop: 20,
  },
  title: {
    fontSize: 20,
    flex: 1,
    fontWeight: 'bold',
    width: '50%',
  },
  subtitle: {
    fontSize: 14,
    flex: 1,
  },
  date: {
    fontSize: 10,
  },
  author: {
    fontSize: 10,
    fontWeight: 'bold',
  },
  headline: {
    fontSize: 25,
    fontWeight: 'bold',
    color: Color.primaryColor,
  },
  image: {
    height: 100,
    width: 150,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    marginLeft: 15,
    position: 'absolute',
    top: -20,
    right: 0,
    borderColor: '#DFEAEF',
    borderWidth: 1,
    backgroundColor: 'white',
  },
  sourceText: { color: 'white' },
  sourceWrapper: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    padding: 5,
    backgroundColor: '#66864A',
    borderBottomRightRadius: 10,
    borderTopLeftRadius: 10,
  },
  footer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingRight: 10,
    marginTop: 10,
  },
  headerWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  loadmore: { alignSelf: 'center' },
});

export default styles;
