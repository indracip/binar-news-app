import HomePage from './home';
import SearchPage from './search';
import NewsDetail from './newsDetail';

export { HomePage, SearchPage, NewsDetail };
