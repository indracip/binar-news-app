# binar-news-app

### REGISTER:
news api: https://newsapi.org/

### Docs:
https://newsapi.org/docs/endpoints

### Page:
1. HOME: {{baseUrl}}/top-headlines?country=id&apiKey={{apiKey}}&page=1
2. SEARCH: {{baseUrl}}/everything?q=telkomsel&page=1&apiKey={{apiKey}}

### Requirement:
1. WebView
2. Unlimited Scroll using FlatList
3. Dalam 1 Card: Title, Description, Image, Date, Author
4. When Seach hit on press button search, keyboardType ```search```